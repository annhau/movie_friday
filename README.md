# movie_friday

A simple poll app (similar to the django tutorial) with some customization.

## Installation
> pip install -r requirement

> cd pythonweb

> python manage.py runserver

> Open in browser: http://localhost:8000/movie/

Account for [admin page](http://127.0.0.1:8000/admin/):
> username: tthanh
> 
> password: 1

## Flow

### Homepage
![img.png](img.png)

### Detail page
![img_2.png](img_2.png)

### Result page
![img_3.png](img_3.png)

### Admin page
![img_1.png](img_1.png)