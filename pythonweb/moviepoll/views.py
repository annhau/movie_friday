from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from .models import Question, Choice


def index(request):
    """
    Index page.
    List of the question (actually movie fridays) and their polls
    :param request:
    :return:
    """
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'moviepoll/index.html', context)


def detail(request, question_id):
    """
    Detail page of a poll
    It shows the poll's choices and a vote button to vote for a choice
    :param request:
    :param question_id:
    :return:
    """
    question = get_object_or_404(Question, pk=question_id)
    choices = question.choice_set.all()
    context = {'question': question, 'choices': choices}
    return render(request, 'moviepoll/detail.html', context)


def results(request, question_id):
    """
    Result page after voted
    It shows number of votes for every choice we have
        and a 'Vote again?' button to get a back to detail page
    :param request:
    :param question_id:
    :return:
    """
    question = get_object_or_404(Question, pk=question_id)
    total_vote = question.choice_set.aggregate(total=Sum('votes'))
    return render(request, 'moviepoll/results.html', {'question': question,
                                                      **total_vote})


def vote(request, question_id):
    """
    This handles submitted request from the vote button at detail page
    If choice is not selected, it goes back to the detail page with an
    error message.
    Else, it increases the votes of the choice by 1 and redirect to the result
    page
    :param request:
    :param question_id:
    :return:
    """
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'moviepoll/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('results', args=(question_id,)))
